SUMMARY = ""CSV" parser for node-red."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-bigcsv"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/Jacques44/node-red-contrib-bigcsv.git;rev=master \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
