SUMMARY = "Rest API definition for Kafka"
LICENSE = "CLOSED"

require ../kafka-common.inc

REST_SERVICE="${PN}.service"
SERVICES="${REST_SERVICE}"

DEPENDS = "libnl openssl zookeeper broker"
INITSCRIPT_NAME = "${REST_SERVICE}"
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${REST_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"


DEFAULT_PREFERENCE = "-1"

SRCREV = "${AUTOREV}"
SRC_URI += "\
        file://${PN}.properties \
        file://${REST_SERVICE} \        
"

do_install() {  
    install -d ${CONFIG_PATH}
    install -m 0755 ${WORKDIR}/*.properties ${CONFIG_PATH}
}
