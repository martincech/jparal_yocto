﻿var http = require('http');
var express = require("express");
var RED = require("node-red");
var os = require('os');

// Create an Express app
var app = express();

// Add a simple route for static content served from 'public'
app.use("/mqtt", express.static(os.homedir() + "/public/topic-tree"));
app.use("/mqtt/admin", express.static(os.homedir() + "/public/mqtt-admin"));


// Create a server
var server = http.createServer(app);

// Create the settings object - see default settings.js file for other options
var settings = {
    httpAdminRoot: "/flows",
    httpNodeRoot: "/",
    ui: { path: "/ui" },
    //adminAuth: {
    //    type: "credentials",
    //    users: [{
    //        username: "admin",
    //        password: "$2a$08$zZWtXTja0fB1pzD4sHCMyOCMYz2Z6dNbM6tl8sJogENOMcxWV9DN.", //password
    //        permissions: "*"
    //    }]
    //},
    //httpNodeAuth: {
    //    user: "admin",
    //    pass: "$2a$08$zZWtXTja0fB1pzD4sHCMyOCMYz2Z6dNbM6tl8sJogENOMcxWV9DN."
    //},
    userDir: os.homedir() + "/.nodered/",
    flowFile: "flows.json",
    functionGlobalContext: {}    // enables global context    
};


// Initialise the runtime with a server and settings
RED.init(server, settings);

// Serve the editor UI from /red
app.use(settings.httpAdminRoot, RED.httpAdmin);

// Serve the http nodes UI from /api
app.use(settings.httpNodeRoot, RED.httpNode);

server.listen(80);

// Start the runtime
RED.start();