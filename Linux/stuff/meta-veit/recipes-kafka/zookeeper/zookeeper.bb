SUMMARY = "Zookeeper for Kafka"
LICENSE = "CLOSED"

require ../kafka-common.inc

ZOOKEEPER_SERVICE="${PN}.service"
SERVICES="${ZOOKEEPER_SERVICE}"

DEPENDS = "libnl openssl"
INITSCRIPT_NAME = "${ZOOKEEPER_SERVICE} "
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${ZOOKEEPER_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

DEFAULT_PREFERENCE = "-1"

SRCREV = "${AUTOREV}"
SRC_URI += "\
	     http://packages.confluent.io/archive/2.0/confluent-2.0.1-2.11.7.tar.gz \	
        file://${PN}.properties \
        file://log4j.properties \
        file://test-log4j.properties \
        file://tools-log4j.properties \
        file://connect-log4j.properties \
        file://${ZOOKEEPER_SERVICE} \        
"
SRC_URI[md5sum] = "04f4cf72ee3afa183b9aa46547bbb29f"
SRC_URI[sha256sum] = "4d11025e4721af64439a5c77c4aef9027fa528cf50b48b54e350eea4208b4a51"

do_install() {  
    install -d ${D}${KAFKA_PATH}
    install -d ${D}${KAFKA_PATH}/bin ${D}${KAFKA_PATH}/share ${D}${KAFKA_PATH}/share/java
    rm -rf ${KAFKA_SOURCE_D}/bin/windows
    install -m 0755 ${KAFKA_SOURCE_D}/bin/* ${D}${KAFKA_PATH}/bin/
    install -d ${D}${KAFKA_PATH}/share/java/confluent-common
    install -m 0755 ${KAFKA_SOURCE_D}/share/java/confluent-common/* ${D}${KAFKA_PATH}/share/java/confluent-common     
    install -d ${D}${KAFKA_PATH}/share/java/kafka 
    install -m 0755 ${KAFKA_SOURCE_D}/share/java/kafka/* ${D}${KAFKA_PATH}/share/java/kafka        
    install -d ${D}${KAFKA_PATH}/share/java/kafka-rest 
    install -m 0755 ${KAFKA_SOURCE_D}/share/java/kafka-rest/* ${D}${KAFKA_PATH}/share/java/kafka-rest
    install -d ${D}${KAFKA_PATH}/share/java/rest-utils 
    install -m 0755 ${KAFKA_SOURCE_D}/share/java/rest-utils/* ${D}${KAFKA_PATH}/share/java/rest-utils
    install -d ${D}${KAFKA_PATH}/share/java/schema-registry
    install -m 0755 ${KAFKA_SOURCE_D}/share/java/schema-registry/* ${D}${KAFKA_PATH}/share/java/schema-registry       

    install -d ${CONFIG_PATH}
    install -m 0755 ${WORKDIR}/*.properties ${CONFIG_PATH}    
}
