SUMMARY = "node-red-contrib-modbus for Node-red"
HOMEPAGE = "https://www.npmjs.com/package/node-red-contrib-modbus"
LICENSE = "CLOSED"

DEPENDS = "node-red modbus-serial"
inherit npm-install-global gulp

SRC_URI="\
        git://github.com/biancode/node-red-contrib-modbus.git;rev=98d4cf54d77903f04bcd049942367c05496d9cf4 \
		file://${PN}.patch \
"
S = "${WORKDIR}/git"

do_install_append() {
  oe_runnpm run build
}

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "

