SUMMARY = "A node-red node to set flow and global context values at start up."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-config"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/DeanCording/node-red-contrib-config.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
