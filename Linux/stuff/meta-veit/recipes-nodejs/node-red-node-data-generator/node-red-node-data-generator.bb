SUMMARY = "A Node-RED node to create a string of dummy data values from a template. Useful for test-cases."
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-data-generator"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/function/datagenerator"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
