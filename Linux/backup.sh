#!/bin/bash

GIT_MOUNT_POINT="/media/sf_BAT3"
GIT_FOLDER="$GIT_MOUNT_POINT/Linux"
TERMINAL_FOLDER="$GIT_MOUNT_POINT/NodeTerminal"
STUFF_FOLDER="$GIT_FOLDER/stuff"
BUILD_FOLDER="$GIT_FOLDER/build"
REVERSE=0

function Usage()
{
	echo ""
	echo "Backup build settings to git and vice versa"
	echo ""
	echo "-h           : prints this help"
	echo "-r           : do reverse, load from git to oe-core"
	echo ""
	echo "Example \"./backup.sh\" copies all the data from oe-core to git"
	echo ""
}

function CopyNodeTerminal()
{
  BACKDIR=`pwd`
  TERMINAL_FILES="public/ nodered/ package.json app.js database.sqlite"
  cd layers/meta-veit/recipes-nodejs/node-terminal/node-terminal/  
  mkdir node-terminal
  cd node-terminal
  for prop in $TERMINAL_FILES ; 
  do
     cp -R "$TERMINAL_FOLDER/$prop" . > /dev/null 2>&1
	 sed -i 's/\r//' $prop > /dev/null 2>&1
  done 
  rm nodered/.*> /dev/null 2>&1
  cd ..
  tar cz node-terminal > node-terminal.tgz
  rm -rf node-terminal
  cd $BACKDIR
}

while getopts rh Option ; do
	case $Option in
		r)	REVERSE=1
			;;
		h)	Usage
			# Exit if only usage (-h) was specified.
			if [ "$#" -eq 1 ] ; then
				exit 10
			fi
			exit 0
			;;
	esac
done


sudo mount -t vboxsf BAT3 $GIT_MOUNT_POINT

if [ $REVERSE -ne 0 ]; then
  echo "Doing reverse backup from git"   
  read -p "Continue (y/n)? " -n 1 -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
      exit 1
  fi
  cd layers
  rm -rf meta-veit
  cp -r $STUFF_FOLDER/* .
  cd ..
  cp $GIT_FOLDER/backup.sh .
  cp $GIT_FOLDER/create_oe-core.sh .
  CopyNodeTerminal
  
  FILES=`ls -R layers/meta-veit | awk '/:$/&&f{s=$0;f=0}/:$/&&!f{sub(/:$/,"");s=$0;f=1;next}NF&&f{ print s"/"$0 }' | grep -v ".tar*"`
  FILES+=" ./backup.sh"
  FILES+=" ./create_oe-core.sh"
  for prop in $FILES ; 
  do
     sed -i 's/\r//' $prop > /dev/null 2>&1
  done  
else
  echo "Doing backup to git"
  read -p "Continue (y/n)? " -n 1 -r
  if [[ ! $REPLY =~ ^[Yy]$ ]]
  then
      exit 1
  fi
  rm -rf $STUFF_FOLDER
  mkdir $STUFF_FOLDER
  cd layers
  find . -name '*~' -exec rm {} \;
  cd ..
  cp -r layers/meta-veit $STUFF_FOLDER
  cp backup.sh $GIT_FOLDER
  cp create_oe-core.sh $GIT_FOLDER
fi