SUMMARY = "A Node-RED node to pack and unpack objects to base64 format"
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-base64"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/parsers/base64"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
