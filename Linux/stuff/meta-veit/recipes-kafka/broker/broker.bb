SUMMARY = "Kafka"
LICENSE = "CLOSED"

require ../kafka-common.inc

BROKER_SERVICE="${PN}.service"
SERVICES="${BROKER_SERVICE}"

DEPENDS = "libnl openssl zookeeper"
INITSCRIPT_NAME = "${BROKER_SERVICE}"
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${BROKER_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

DEFAULT_PREFERENCE = "-1"

SRCREV = "${AUTOREV}"
SRC_URI += "\
        file://${PN}.properties \
        file://${BROKER_SERVICE} \    
"

do_install() {  
    install -d ${CONFIG_PATH}
    install -m 0755 ${WORKDIR}/*.properties ${CONFIG_PATH}
}
