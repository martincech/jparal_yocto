SUMMARY = "A Node-RED node that uses Noble to interact with BLEs"
HOMEPAGE = "http://flows.nodered.org/node/node-red-contrib-noble"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/kmi/node-red-contrib-noble.git;rev=master \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
