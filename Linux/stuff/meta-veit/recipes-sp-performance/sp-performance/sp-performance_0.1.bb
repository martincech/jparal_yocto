SUMMARY = "Sensor pack data collector"
LICENSE = "CLOSED"

SP_PERFORMANCE_SERVICE="SpPerformance.service"
SERVICES="${SP_PERFORMANCE_SERVICE}"

DEPENDS = "libnl openssl mono"
INITSCRIPT_NAME = "${SP_PERFORMANCE_SERVICE}"
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${SP_PERFORMANCE_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "disable"
SP_PERFORMANCE="KafkaPerformanceTests.exe"

SRC_URI = "\	
        file://SpPerformance.tar \
        file://SpPerformance.service \	
"
PERFORMANCE_PATH="/usr/sp-performance"
S_B ="${D}${PERFORMANCE_PATH}"
S_B_SCOURCE_D="${WORKDIR}/SpPerformance"

do_install() {
    install -d ${S_B}  
    install -m 0755 ${S_B_SCOURCE_D}/* ${S_B}/ 
    
    install -d ${D}${systemd_unitdir}/system    
    install -m 0755 ${WORKDIR}/${SP_PERFORMANCE_SERVICE} ${D}${systemd_unitdir}/system/

    for prop in ${D}${systemd_unitdir}/system/${SP_PERFORMANCE_SERVICE}; 
    do
      sed -i -e "s,@PERFORMANCE_PATH@,"${PERFORMANCE_PATH}",g" -e "s,@SP_PERFORMANCE@,"${SP_PERFORMANCE}",g" ${prop}
    done    
}

FILES_${PN} = "\   
  ${PERFORMANCE_PATH}/* \"
