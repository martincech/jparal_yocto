﻿CREATE TABLE device (
	device TEXT NOT NULL PRIMARY KEY, 
	type TEXT NOT NULL, 
	name TEXT);
CREATE TABLE sensor_data (
	epoch INTEGER NOT NULL DEFAULT CURRENT_TIMESTAMP, 
	device TEXT NOT NULL, 
	sensor TEXT NOT NULL, 
	value REAL, 
	PRIMARY KEY(epoch, device, sensor), 
	FOREIGN KEY (device) REFERENCES device(device));
CREATE TABLE sensor_aggr (
	epoch INTEGER NOT NULL, 
	device TEXT NOT NULL, 
	sensor TEXT NOT NULL, 
	aggrtype TEXT NOT NULL, 
	interval INTEGER NOT NULL, 
	value REAL, 
	PRIMARY KEY(epoch, device, sensor, aggrtype, interval), 
	FOREIGN KEY (device) REFERENCES device(device));
CREATE TABLE per_topic_aggr (
	device TEXT NOT NULL, 
	sensor TEXT NOT NULL, 
	aggrtype TEXT NOT NULL,  
	PRIMARY KEY(device, sensor, aggrtype),
	FOREIGN KEY(device) REFERENCES device(device));
CREATE TABLE aggr_interval(minute_interval INTEGER NOT NULL PRIMARY KEY);
CREATE TABLE topic_aggr_interval(
	device TEXT NOT NULL, 
	sensor TEXT NOT NULL, 
	aggrtype TEXT NOT NULL, 
	minute_interval INTEGER NOT NULL,
	PRIMARY KEY(device, sensor, aggrtype, minute_interval),
	FOREIGN KEY(device, sensor, aggrtype) REFERENCES per_topic_aggr(device, sensor, aggrtype),
	FOREIGN KEY(minute_interval) REFERENCES aggr_interval(minute_interval));



-- intervals of data aggregation
INSERT INTO aggr_interval (minute_interval) VALUES (30); --half hour
INSERT INTO aggr_interval (minute_interval) VALUES (60); --hour
INSERT INTO aggr_interval (minute_interval) VALUES (240); --4 hours
INSERT INTO aggr_interval (minute_interval) VALUES (480); --8 hours
INSERT INTO aggr_interval (minute_interval) VALUES (720); --half day - 12 hours
INSERT INTO aggr_interval (minute_interval) VALUES (1440); --a day - 24 hours


--CREATE TABLE curve (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Name TEXT NOT NULL COLLATE NOCASE);
--CREATE TABLE curve_point (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, day INTEGER NOT NULL, value REAL,curveid INTEGER NOT NULL, FOREIGN KEY (curveid) REFERENCES curve(id));
--CREATE TABLE event (
--	id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
--	name TEXT NOT NULL COLLATE NOCASE,
--	from datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
--	to datetime NOT NULL,	
--	start_age integer NOT NULL DEFAULT 1,	
--	stabilization_range float NOT NULL DEFAULT 3,
--	stabilization_window_sample_count integer NOT NULL DEFAULT 10,
--	filtration_averaging_window integer NOT NULL DEFAULT 10,
--	acceptance_above_male float NOT NULL DEFAULT 30,
--	acceptance_below_male float NOT NULL DEFAULT 30,
--	acceptance_above_female float NOT NULL DEFAULT 30,
--	acceptance_below_female float NOT NULL DEFAULT 30,
--	stepmode smallint NOT NULL,
--	sexmode smallint NOT NULL,	
--	weighmode	smallint NOT NULL,
--	initial_weight	float NOT NULL,
--	initial_weight_female	float NOT NULL,
--	curveid integer,curvefemaleid	integer, FOREIGN KEY (curveid) REFERENCES curve(id), FOREIGN KEY (curvefemaleid) REFERENCES curve(id)
--);

