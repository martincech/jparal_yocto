SUMMARY = "A Node-RED graphing package. Contains a datasource node which handles historical data and live data streams, and a hackable visualization application designed to connect to the datasource nodes."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-graphs"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/IBM-IoT/node-red-contrib-graphs.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
