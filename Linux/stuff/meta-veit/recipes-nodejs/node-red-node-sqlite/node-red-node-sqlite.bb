SUMMARY = "A sqlite node for Node-RED"
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-sqlite"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/storage/sqlite"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
