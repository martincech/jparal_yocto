SUMMARY = "XBee node for use with Node-Red"
HOMEPAGE = "https://github.com/freakent/NR-XBee"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/freakent/NR-XBee.git;rev=master \
		file://package.json \
"
S = "${WORKDIR}/git"

do_install_prepend() {
  cd ${WORKDIR}
  cp package.json ${S}
}

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "