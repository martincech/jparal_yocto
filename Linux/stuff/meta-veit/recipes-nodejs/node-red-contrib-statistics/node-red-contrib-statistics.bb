SUMMARY = "A node-red node to perform simple statistical operations on a flow."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-statistics"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        file://statistics.js \
		file://statistics.html \
		file://package.json \
"
S = "${WORKDIR}"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
