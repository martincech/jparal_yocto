SUMMARY = "Node-RED nodes to talk to serial ports"
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-serialport"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/io/serialport"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
