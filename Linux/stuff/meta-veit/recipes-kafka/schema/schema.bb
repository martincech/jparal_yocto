SUMMARY = "Schema registry definition for Kafka"
LICENSE = "CLOSED"

require ../kafka-common.inc

SCHEMA_SERVICE="${PN}.service"
SERVICES="${SCHEMA_SERVICE}"

DEPENDS = "libnl openssl zookeeper broker"
INITSCRIPT_NAME = "${SCHEMA_SERVICE} "
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${SCHEMA_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"


DEFAULT_PREFERENCE = "-1"

SRCREV = "${AUTOREV}"
SRC_URI += "\
        file://${PN}.properties \
        file://${SCHEMA_SERVICE} \        
"

do_install() {  
    install -d ${CONFIG_PATH}
    install -m 0755 ${WORKDIR}/*.properties ${CONFIG_PATH}   
}
