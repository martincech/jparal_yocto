SUMMARY = "Samba mounting of local VEIT disk for deployment"
HOMEPAGE = "www.veit.cz"
LICENSE = "CLOSED"

HOME_DIR="/home/root"

SRC_URI="\
		file://${PN}.sh \
"

do_install(){
  install -d ${D}${HOME_DIR} 
  install -m 0755 ${WORKDIR}/${PN}.sh ${D}${HOME_DIR} 
}

FILES_${PN} = "\   
     ${HOME_DIR}/* \
"

