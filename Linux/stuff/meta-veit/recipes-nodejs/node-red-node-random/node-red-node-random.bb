SUMMARY = "A Node-RED node that when triggered generates a random number between two values."
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-random"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/function/random"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
