SUMMARY = "Node-RED nodes of HighLevel Kafka Producer and Consumer"
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-kafka-node"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/fwang7/node-red-contrib-kafka-node.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
