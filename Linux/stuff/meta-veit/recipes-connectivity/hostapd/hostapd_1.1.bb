HOMEPAGE = "http://hostap.epitest.fi"
SECTION = "kernel/userland"
LICENSE = "GPLv2 | BSD"
LIC_FILES_CHKSUM = "file://README;md5=4d709ce0f9c84b87d148e16731f647e1"
DEPENDS = "libnl openssl"
SUMMARY = "User space daemon for extended IEEE 802.11 management"

inherit update-rc.d systemd
INITSCRIPT_NAME = "hostapd"

SYSTEMD_SERVICE_${PN} = "access-point.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

DEFAULT_PREFERENCE = "-1"

VER = "1.1"

SRC_URI = " \
    http://hostap.epitest.fi/releases/hostapd-${VER}.tar.gz \
    file://defconfig \
    file://access-point \
    file://access-point.service \
    file://hostapd.conf \   
    file://udhcpd.conf \  
    file://udhcpd.leases \    
"

S = "${WORKDIR}/hostapd-${VER}/hostapd"

AP_IP = "192.168.0.1"
AP_BROADCAST = "192.168.0.255"
#Base for ssid (example ssid : VEIT-AP_08b300a3c4a1)
AP_SSID = "VEIT-AP" 
#Bae for ap password (example password : veit-ap_c4a1)
AP_PASSWORD = "veit-ap"

AP_DHCP_START = "192.168.0.20"
AP_DHCP_END = "192.168.0.254"

do_configure() {
    install -m 0644 ${WORKDIR}/defconfig ${S}/.config
}

do_compile() {
    export CFLAGS="-MMD -O2 -Wall -g -I${STAGING_INCDIR}/libnl3"
    make
}

do_install() {   
    install -d ${D}${sysconfdir}
    #copy udhcpd conf file
    install -m 0755 ${WORKDIR}/udhcpd.conf ${D}${sysconfdir}
    sed -i -e 's,@AP_IP@,${AP_IP},g' -e 's,@AP_DHCP_START@,${AP_DHCP_START},g' -e 's,@AP_DHCP_END@,${AP_DHCP_END},g' ${D}${sysconfdir}/udhcpd.conf


    #copy hostapd conf file
    install -m 0755 ${WORKDIR}/hostapd.conf ${D}${sysconfdir}	
    sed -i -e 's,@AP_PASSWORD@,${AP_PASSWORD},g' -e 's,@AP_SSID@,${AP_SSID},g' ${D}${sysconfdir}/hostapd.conf
    
    install -d ${D}${sbindir}    
    install -m 0755 ${S}/hostapd ${D}${sbindir}
    install -m 0755 ${S}/hostapd_cli ${D}${sbindir}

    install -d ${D}${localstatedir}/lib/misc
    install -m 0755 ${WORKDIR}/udhcpd.leases ${D}${localstatedir}/lib/misc/   
    install -m 0755 ${WORKDIR}/access-point ${D}${localstatedir}/lib/misc/
    sed -i -e 's,@AP_IP@,${AP_IP},g' -e 's,@AP_BROADCAST@,${AP_BROADCAST},g' -e 's,@AP_PASSWORD@,${AP_PASSWORD},g' -e 's,@AP_SSID@,${AP_SSID},g' ${D}${localstatedir}/lib/misc/access-point

    install -d ${D}${systemd_unitdir}/system    
    install -m 0644 ${WORKDIR}/access-point.service ${D}${systemd_unitdir}/system/
    #sed -i -e 's,@SBINDIR@,${sbindir},g' -e 's,@SYSCONFDIR@,${sysconfdir},g' ${D}${systemd_unitdir}/system/access-point.service
}

CONFFILES_${PN} += "${sysconfdir}/hostapd.conf"

SRC_URI[md5sum] = "e3ace8306d066ab2d24b4c9f668e2dd7"
SRC_URI[sha256sum] = "d1ec8d2550f4f59d7611d8de68a61936cd3645fffdd2854c5b2bc758ece3b66c"
