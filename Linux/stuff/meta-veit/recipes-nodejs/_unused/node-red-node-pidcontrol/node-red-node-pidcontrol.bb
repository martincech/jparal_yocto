SUMMARY = "A Node-RED node that that provides a simple PID controller."
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-pidcontrol"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/function/PID"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
