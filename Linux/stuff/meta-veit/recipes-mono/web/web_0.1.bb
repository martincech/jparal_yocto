SUMMARY = "Terminal web interface"
LICENSE = "CLOSED"

DEPENDS = "mono mono-xsp"

require ../web-common.inc

SRC_URI = "\	
        file://LinuxWeb.tar \       
"

do_install() {  
   install -d ${D}${WEB_PATH} 
   cp -r ${WORKDIR}/LinuxWeb/* ${D}${WEB_PATH}/ 
}
