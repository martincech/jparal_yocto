#!/bin/bash


function Usage()
{
	echo ""
	echo "Builds Colibri iMX6 and prepares the final image"
	echo ""
	echo "-h           : prints this help"
	echo "-b           : bitbake and prepare, otherwise use the latest build"
	echo "-o           : bitbake without the meta-veit layer"
	echo ""
	echo "Example \"./build.sh -b\" bitbakes and prepares the output image"
	echo ""
}

# trap ctrl-c and call ctrl_c()
trap ctrl_c SIGINT

function ctrl_c() {
  echo "Build interrupted"
  exit 3
}

# initialise options
CUR_DIR="$PWD"
OE_DIR="$PWD/../../"
BUILD_DIR="$OE_DIR/build"
COLIBRI_IMAGE="Colibri_iMX6_LinuxConsoleImageV2.6.1"
OUTPUT_MOUNT_FOLDER="/media/sf_shared"
BITBAKE=0
IMAGE="console-trdx-image"

while getopts obh Option ; do
	case $Option in
		b)	BITBAKE=1
			;;
		h)	Usage
			# Exit if only usage (-h) was specified.
			if [ "$#" -eq 1 ] ; then
				exit 10
			fi
			exit 0
			;;
	esac
done

OUTPUT_SD_CARD_FOLDER="$OUTPUT_MOUNT_FOLDER/toradex_linux_veit"

echo "Copying configuration to build directory"
if [ ! -d $BUILD_DIR ]; then
   mkdir $BUILD_DIR
fi
if [ ! -d $BUILD_DIR/conf ]; then
   mkdir $BUILD_DIR/conf
fi

cp conf/bblayers.conf $BUILD_DIR/conf/bblayers.conf
cp conf/local.conf $BUILD_DIR/conf/local.conf

if [ $BITBAKE -ne 0 ]; then
export ARCH=arm
export PATH=~/oe-core/build/tmp-glibc/sysroots/x86_64-linux/usr/bin:~/oe-core/build/tmp-glibc/sysroots/x86_64-linux/usr/bin/arm-angstrom-linux-gnueabi/:$PATH
export CROSS_COMPILE=arm-angstrom-linux-gnueabi-
  echo "Building image "$COLIBRI_IMAGE
  cd $OE_DIR
  . export
  bitbake -k $IMAGE
fi

#enter the output directory
cd $OE_DIR/deploy/images/colibri-imx6
if [ -d "$COLIBRI_IMAGE" ]; then	
  echo "Removing old build"
  sudo rm -rf $COLIBRI_IMAGE
fi

echo "Extracting image"
sudo tar -xvvf `ls -t $COLIBRI_IMAGE* | head -1` > /dev/null

cd $COLIBRI_IMAGE
echo "Checking image content"


#rtl drivers
if [ -d "rootfs/lib/firmware/rtlwifi" ]; then
#delete all other drivers from the lib/firmware
sudo mv rootfs/lib/firmware/rtlwifi .
sudo mv rootfs/lib/firmware/htc_9271.fw .
sudo mv rootfs/lib/firmware/rt2870.bin .
sudo rm -rf rootfs/lib/firmware/*
sudo mv rtlwifi rootfs/lib/firmware/
sudo mv htc_9271.fw rootfs/lib/firmware/
sudo mv rt2870.bin rootfs/lib/firmware/
echo "RTLWIFI OK"
else
echo "!!!RTLWIFI drivers not installed!!!"  
exit 2
fi


#configuration of hostapd
if [ ! -f "rootfs/etc/hostapd.conf" ]; then
  echo "!!!Hostapd configuration not found!!!"  
  exit 2
fi

#if [ ! -f "rootfs/lib/systemd/system/broker.service" ]; then  
#  echo "!!!broker.service not foud in /lib/systemd/system/!!!" 
#fi

#if [ ! -d "rootfs/usr/kafka" ]; then 
#  echo "!!!Apache kafka folder not found in /usr/kafka!!!"  
#fi

#if [ ! -d "rootfs/usr/kafka/bin" ]; then  
#  echo "!!!Apache kafka bin not found in /usr/kafka/bin!!!"  
#fi

#if [ ! -f "usr/bin/node-red" ]; then  
#  echo "!!!node-red not found in usr/bin/node_modules/!!!"  
#fi

echo "Mounting output folder"
sudo mount -t vboxsf shared $OUTPUT_MOUNT_FOLDER
if [ -d "$OUTPUT_SD_CARD_FOLDER" ]; then
  rm -rf $OUTPUT_SD_CARD_FOLDER
fi
mkdir $OUTPUT_SD_CARD_FOLDER

echo "Creating output image"
./update.sh -o $OUTPUT_SD_CARD_FOLDER > /dev/null

echo "Files writen to "$OUTPUT_SD_CARD_FOLDER
echo "Copy content to SD card and execute on toradex board these commands:"
echo "run setupdate"
echo "run update"

cd $CUR_DIR
