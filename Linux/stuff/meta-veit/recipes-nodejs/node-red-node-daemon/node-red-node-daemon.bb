SUMMARY = "A Node-RED node that runs and monitors a long running system command."
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-daemon"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/utility/daemon"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
