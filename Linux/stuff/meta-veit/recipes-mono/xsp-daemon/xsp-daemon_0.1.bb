SUMMARY = "Web server daemon for terminal configuration"
SECTION = "kernel/userland"
LICENSE = "CLOSED"

require ../web-common.inc

XSP_SERVICE="${PN}.service"
SERVICES="${XSP_SERVICE}"

DEPENDS = "mono mono-xsp"
INITSCRIPT_NAME = "${XSP_SERVICE}"
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${XSP_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

DEFAULT_PREFERENCE = "-1"

SRCREV = "${AUTOREV}"
SRC_URI = " \   
    file://${XSP_SERVICE} \
    file://web.webapp \  
"

MONO_XSP_PATH="/usr/lib/xsp"
XSPSERVER="/usr/bin/xsp4"


do_install() {
    install -d 0755 ${D}${MONO_XSP_PATH}/ ${D}${systemd_unitdir}/system/
   
    install -m 0644 ${WORKDIR}/${XSP_SERVICE} ${D}${systemd_unitdir}/system/
    install -m 0755  ${WORKDIR}/web.webapp ${D}${MONO_XSP_PATH}/

    for prop in ${D}${systemd_unitdir}/system/${XSP_SERVICE} ${D}${MONO_XSP_PATH}/web.webapp; 
    do
      sed -i -e "s,@MONO_XSP_PATH@,"${MONO_XSP_PATH}",g" ${prop}
      sed -i -e "s,@XSPSERVER@,"${XSPSERVER}",g" ${prop}
      sed -i -e "s,@WEB_PATH@,"${WEB_PATH}",g" ${prop}
    done
}

FILES_${PN} = "\ 
   ${MONO_XSP_PATH}/* \
   ${systemd_unitdir}/system/* \"
