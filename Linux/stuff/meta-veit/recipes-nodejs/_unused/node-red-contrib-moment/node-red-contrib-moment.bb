SUMMARY = "Node-Red Node that produces formatted Date/Time output using the Moment.JS library. Timezone, dst and locale aware."
HOMEPAGE = "http://flows.nodered.org/node/node-red-contrib-moment"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/TotallyInformation/node-red-contrib-moment.git;rev=master \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
