/**
 * Copyright 2016 Dean Cording
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

// for function docu see http://simplestatistics.org/docs
module.exports = function(RED) {

    var statistics = require("simple-statistics");


    function StatisticsNode(config) {
        RED.nodes.createNode(this, config);
        this.stripFunction = config.stripFunction;
        var node = this;
        this.on("input", function(msg) {

            var context = this.context();
            var dataset = msg.dataset || "data";
            var data = context.get(dataset) || [];
            var value = null;
            var value2 = null;
            var value3 = null;
            // update dataset with wavlue            
            if (msg.payload != null) {
                value = parseFloat(msg.payload);
                if (isNaN(value)) {
                    node.warn("Non-numeric data received: " + msg.payload);
                    return null;
                } else {
                    data.push(value);
                }
            }

            //do some function if available
            var functions = msg.functions;
            if (!functions instanceof Array) {
                //single function
                functions = [functions];
            }

            var topic = msg.topic;
            var result = null;
            for (var i = 0; i < functions.length; i++) {
                var func = functions[i];
                switch (func.name) {
                    // special functions not in simple statistics
                    case "clear":
                        data = [];
                        break;

                    case "size":
                        result = data.length;
                        break;
                    // functions with 0 parameter and data
                    case "min":
                    case "max":
                    case "sum":
                    case "sumSimple":
                    case "product":
                    case "minSorted":
                    case "maxSorted":
                    case "mean":
                    case "mode":
                    case "modeSorted":
                    case "median":
                    case "medianSorted":
                    case "harmonicMean":
                    case "geometricMean":
                    case "rootMeanSquare":
                    case "sampleSkewness":
                    case "variance":
                    case "sampleVariance":
                    case "standardDeviation":
                    case "sampleStandardDeviation":
                    case "medianAbsoluteDeviation":
                    case "interquartileRange":
                    case "shuffle":
                    case "shuffleInPlace":
                    case "uniqueCountSorted":
                    case "permutationsHeap":
                        result = statistics[func.name](data);
                        break;
                    case "uniqueCount":
                        result = statistics['uniqueCountSorted'](data);
                        break;

                    // functions with 1 parameter and data
                    case "quantile":
                    case "quantileSorted":
                    case "sumNthPowerDeviations":
                    case "sampleCorrelation":
                    case "sampleCovariance":
                    case "sample":
                    case "tTest":
                    case "ckmeans":
                    case "equalIntervalBreaks":
                    case "chunk":
                    case "combinations":
                    case "combinationsReplacement":
                        value = parseFloat(func.parameter);
                        if (isNaN(value)) {
                            node.warn("Non-numeric parameter to function "+ func.name + " received: " + func.parameter);
                        } else {
                            result = statistics[func.name](data, value);
                        }
                        break;
                    case "rSquared":
                        if (!typeof func.parameter === "function") {
                            node.warn("Non-function parameter to function " + func.name + " received: " + func.parameter);
                        } else {
                            result = statistics[func.name](data, func.parameter);
                        }
                        break;

                    // functions without data
                    //single parameter
                    case "linearRegression":
                    case "bernoulliDistribution":
                    case "poissonDistribution":
                    case "cumulativeStdNormalProbability":
                    case "errorFunction":
                    case "inverseErrorFunction":
                    case "probit":
                    case "factorial":
                        result = statistics[func.name](func.parameter);
                        break;
                    case "linearRegressionLine":
                        result = statistics.linearRegression(func.parameter);
                        break;
                    //two parameters
                    case "binomialDistribution":
                        value = parseFloat(func.parameter);
                        value2 = parseFloat(func.parameter2);
                        if (isNaN(value) || isNaN(value2) ) {
                            node.warn("Non-numeric parameter to function " + func.name + " received: " + value + " " + value2);
                        } else {
                            result = statistics[func.name](value, value2);
                        }
                        break;
                    // more parameters
                    case "zScore":
                        value = parseFloat(func.parameter);
                        value2 = parseFloat(func.parameter2);
                        value3 = parseFloat(func.parameter3);
                        if (isNaN(value) || isNaN(value2) || isNaN(value3)) {
                            node.warn("Non-numeric parameter to function " + func.name + " received: " + value + " " + value2 + " " + value3);
                        } else {
                            result = statistics[func.name](value, value2, value3);
                        }
                        break;

                     // others
                    default:
                        node.warn("Undefined function " + func.name);
                        result = null;
                        break;
                }
                msg = {'topic': topic + "/" + func.name, 'payload': result }                
                node.send(msg);
            }
            context.set(dataset, data);
            return null;
        });
    }

    RED.nodes.registerType("statistics", StatisticsNode);
}
