Slo�ka LINUX
============
Tato slo�ka obsahuje soubory nutn� pro vybuildov�n� Linuxu pro desky Toradex.

Build je zalo�en na build syst�mu OpenEmbedded.
Slo�ka obsahuje skripty pro jednoduch� buildov�n� na syst�mu Linux. Pro jednoduchost
je nutn� pou��t virtu�ln� ma�inu s p�ep�ipraven�m Ubuntu. Lze v�ak i na �istou 
novou ma�inu se syst�mem Ubuntu. Skripty nainstaluj� v�e pot�ebn� a umo�n� build.

create_oe-core.sh : 
-------------------
Na �ist�m syst�mu spou�t�no jako prvn�. Nutn� p�ipojit tuto
slo�ku jako disk do virtu�ln�ho stroje na adresu /media/sf_BAT3 (viz obsah skript�)
D�le vytvo�it n�kde v syst�mu Linux slo�ku, do kter� se vytvo�� cel� build image,
tzn. st�hnout se zdroj�ky bude se tam buildovat.
Zde sta�� zkop�rovat skript create_oe-core.sh a spustit.

backup.sh :
-----------
Ukl�d�n� zm�n v build stromu na syst�mu Linux do gitu a zp�t. P�ekop�rov�v�
soubory a adres��e ze slo�ky gitu do c�lov� slo�ky s buildem - viz meta-veit.

stuff
-----
Obsahuje zm�ny pro build Linuxu na konkr�tn� desky pro konkr�tn� pou�it� - update
origin�ln�ho Toradex image.
