SUMMARY = "Run PHP scripts in node"
HOMEPAGE = "https://github.com/mkschreder/node-php"
LICENSE = "CLOSED"

DEPENDS = "nodejs"

#install npm package
inherit npm-install-global

SRC_URI="\
        git://github.com/mkschreder/node-php.git;rev=master \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "

