SUMMARY = "Node terminal application for VEIT terminal"
LICENSE = "CLOSED"

SERVICE="${PN}.service"
DEPENDS = "nodejs node-red \
    express \
    node-red \
"

inherit update-rc.d systemd
#run as service
INITSCRIPT_NAME = "${SERVICE}"
SYSTEMD_SERVICE_${PN} = "${SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"
DEFAULT_PREFERENCE = "-1"

APP_PATH="${libdir}/node_modules/${PN}"
HOME_DIR="/home/root"

SRC_URI="\
   file://${PN}.tgz \
   file://${SERVICE} \
"

do_install_prepend() {
  sed -i -e "s,@APP_PATH@,"${APP_PATH}",g" ${WORKDIR}/${SERVICE}
  install -d ${D}${APP_PATH} 
  install -d ${D}${HOME_DIR}
  install -d ${D}${HOME_DIR}/.nodered
  mv ${WORKDIR}/${PN}/nodered/* ${D}${HOME_DIR}/.nodered
  mv ${WORKDIR}/${PN}/public/ ${D}${HOME_DIR}/
  mv ${WORKDIR}/${PN}/database.sqlite ${D}${HOME_DIR}/

  cp -r ${WORKDIR}/${PN}/* ${D}${APP_PATH} 
  install -d ${D}${systemd_unitdir}/system  
  install -m 0755 ${WORKDIR}/${SERVICE} ${D}${systemd_unitdir}/system/ 
}


FILES_${PN} = "\   
  ${APP_PATH}/* \
  ${HOME_DIR}/.nodered \
  ${HOME_DIR}/public \
  ${HOME_DIR}/database.sqlite \
  ${systemd_unitdir}/system/* \
"
