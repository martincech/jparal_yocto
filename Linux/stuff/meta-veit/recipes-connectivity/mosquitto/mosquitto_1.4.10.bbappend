FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += " \  
   file://mosquitto.conf \  
   file://mosquitto.patch \
"
DEPENDS += "libwebsockets"

do_install_append(){
	install -d ${D}/etc/mosquitto/
	install -m 0755 ${WORKDIR}/mosquitto.conf ${D}/etc/mosquitto/
}
