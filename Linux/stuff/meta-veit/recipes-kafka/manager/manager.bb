SUMMARY = "Kafka manager definition for Kafka"
LICENSE = "CLOSED"

require ../kafka-common.inc

MANAGER_SERVICE="${PN}.service"
SERVICES="${MANAGER_SERVICE}"

DEPENDS = "libnl openssl zookeeper broker"
INITSCRIPT_NAME = "${MANAGER_SERVICE} "
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${MANAGER_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"


DEFAULT_PREFERENCE = "-1"

SRCREV = "${AUTOREV}"
SRC_URI += "\
        file://kafka-manager.tar \
        file://application.conf \
        file://${MANAGER_SERVICE} \        
"

MANAGER="kafka-manager"
MANAGER_SOURCE_D = "${WORKDIR}/${MANAGER}"
M_B = "${D}/KAFKA_PATH/${MANAGER}"

do_install() {  
    install -d ${CONFIG_PATH}
    install -m 0755 ${WORKDIR}/*.conf ${CONFIG_PATH}      

    install -d ${M_B} ${M_B}/bin ${M_B}/conf ${M_B}/lib
    install -m 0755 ${MANAGER_SOURCE_D}/bin/* ${M_B}/bin
    install -m 0755 ${MANAGER_SOURCE_D}/conf/* ${M_B}/conf
    install -m 0755 ${MANAGER_SOURCE_D}/lib/* ${M_B}/lib
}
