SUMMARY = "A Node-RED node that provides several simple smoothing algorithms for incoming data values."
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-smooth"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/function/smooth"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
