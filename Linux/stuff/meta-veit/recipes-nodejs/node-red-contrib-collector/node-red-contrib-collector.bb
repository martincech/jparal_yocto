SUMMARY = "Collect topic/payload pairs and output all values as an object"
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-collector"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/njh/node-red-contrib-collector.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
