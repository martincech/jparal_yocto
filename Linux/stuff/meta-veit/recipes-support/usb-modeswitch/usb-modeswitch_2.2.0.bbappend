FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \ 
   file://usb_modeswitch.conf \ 
"

do_install_append(){
	install -d ${D}/etc	
	install -m 0755 ${WORKDIR}/usb_modeswitch.conf ${D}/etc/
}
