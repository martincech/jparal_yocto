SUMMARY = "A Node-RED node to query PostgreSql"
HOMEPAGE = "http://flows.nodered.org/node/node-red-contrib-postgres"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/krisdaniels/node-red-contrib.git;rev=master \
"
S = "${WORKDIR}/git/${PN}"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
