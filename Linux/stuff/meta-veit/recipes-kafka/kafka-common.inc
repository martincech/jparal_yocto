
KAFKA_PATH="/usr/kafka"
KAFKA_PATH_CONF="${KAFKA_PATH}/config"
MANAGER="kafka-manager"

BROKER_HEAP_SIZE="128"
ZOOKEEPER_HEAP_SIZE="128"
SCHEMA_HEAP_SIZE="16"
REST_HEAP_SIZE="32"
MANAGER_HEAP_SIZE="32"
MIRROR_HEAP_SIZE="48"

BROKER_PORT="9092"
ZOOKEEPER_PORT="2181"
MANAGER_PORT="8080"
SCHEMA_PORT="8081"
REST_PORT="8082"
JMX_PORT="8083"

LOCAL_ADDRESS="localhost"
REMOTE_ADDRESS="192.168.1.216"

KAFKA_SOURCE_D = "${WORKDIR}/confluent-2.0.1"
CONFIG_PATH="${D}${KAFKA_PATH_CONF}"
SED_FILES=""

do_install_append() {  
    for prop in ${WORKDIR}/${SERVICES} ${CONFIG_PATH}/* ${SED_FILES}; 
    do
      sed -i -e "s,@KAFKA_PATH@,"${KAFKA_PATH}",g" -e "s,@MANAGER@,"${MANAGER}",g" -e "s,@CONFIG_PATH@,"${KAFKA_PATH_CONF}",g" ${prop}
      sed -i -e "s,@BROKER_PORT@,"${BROKER_PORT}",g" -e "s,@BROKER_HEAP_SIZE@,"${BROKER_HEAP_SIZE}",g" ${prop}
      sed -i -e "s,@ZOOKEEPER_PORT@,"${ZOOKEEPER_PORT}",g" -e "s,@ZOOKEEPER_HEAP_SIZE@,"${ZOOKEEPER_HEAP_SIZE}",g" ${prop}
      sed -i -e "s,@SCHEMA_PORT@,"${SCHEMA_PORT}",g" -e "s,@REST_PORT@,"${REST_PORT}",g" ${prop}
      sed -i -e "s,@SCHEMA_HEAP_SIZE@,"${SCHEMA_HEAP_SIZE}",g" -e "s,@REST_HEAP_SIZE@,"${REST_HEAP_SIZE}",g" ${prop}
      sed -i -e "s,@MANAGER_HEAP_SIZE@,"${MANAGER_HEAP_SIZE}",g" -e "s,@MIRROR_HEAP_SIZE@,"${MIRROR_HEAP_SIZE}",g" ${prop}
      sed -i -e "s,@MANAGER_PORT@,"${MANAGER_PORT}",g" -e "s,@JMX_PORT@,"${JMX_PORT}",g" ${prop}
      sed -i -e "s,@MANAGER_PORT@,"${MANAGER_PORT}",g" -e "s,@JMX_PORT@,"${JMX_PORT}",g" ${prop}
      sed -i -e "s,@LOCAL_ADDRESS@,"${LOCAL_ADDRESS}",g" -e "s,@REMOTE_ADDRESS@,"${REMOTE_ADDRESS}",g" ${prop}
      sed -i -e "s,@MIRROR_MAKER_PATH@,"${MIRROR_MAKER_PATH}",g" -e "s,@MIRROR_MAKER@,"${MIRROR_MAKER}",g" ${prop}
    done

    install -d ${D}${systemd_unitdir}/system  
    for prop in ${SERVICES}; 
    do
      install -m 0755 ${WORKDIR}/${prop} ${D}${systemd_unitdir}/system/ 
    done
}

FILES_${PN} = "\   
   ${KAFKA_PATH}/* \      
   ${systemd_unitdir}/system/* \"
