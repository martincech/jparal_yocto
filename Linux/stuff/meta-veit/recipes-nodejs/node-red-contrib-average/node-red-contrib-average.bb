SUMMARY = "A Node-RED node to calculate average."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-average"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/eisbehr-/node-red-average.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
