SUMMARY = "Node-RED node to scan your network."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-wifiscan"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/arlemi/node-red-contrib-wifiscan.git;rev=master \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
