SUMMARY = "Node-Red module for Apache Kafka publish/subscribe using the Confluent librdkafka C library"
HOMEPAGE = "https://www.npmjs.com/package/node-red-contrib-rdkafka"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/hjespers/node-red-contrib-rdkafka.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
