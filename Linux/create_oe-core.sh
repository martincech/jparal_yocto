#!/bin/bash

function Usage()
{
	echo ""
	echo "Download and create whole oe-core to build Linux image."
	echo ""
	echo "-h           : prints this help"
	echo ""
	echo ""
}

while getopts bh Option ; do
	case $Option in
		h)	Usage
			# Exit if only usage (-h) was specified.
			if [ "$#" -eq 1 ] ; then
				exit 10
			fi
			exit 0
			;;
	esac
done

TORADEX_SOURCES="http://git.toradex.com/toradex-bsp-platform.git"
LINUX_VERSION="LinuxImageV2.6.1"
MONO_SOURCES="git://git.yoctoproject.org/meta-mono"
META_JAVA_BRANCH=33ddb28a6428b02ddcc82d1954ecf27cd426fbb5
META_JAVA_SOURCES="git://git.yoctoproject.org/meta-java"
NODE_SOURCES="https://github.com/imyller/meta-nodejs.git"
NODE_CONTRIBS="https://github.com/imyller/meta-nodejs-contrib.git"
MOSQUITTO_SOURCES="git://git.yoctoproject.org/meta-intel-iot-middleware"
# trap ctrl-c and call ctrl_c()
trap ctrl_c SIGINT

function ctrl_c() {
  echo "oe-core creation interrupted"
  exit 3
}

repo init -u $TORADEX_SOURCES -b $LINUX_VERSION
repo sync

#clone stuff from our git repository
GIT_MOUNT_POINT="/media/sf_BAT3"
GIT_FOLDER="$GIT_MOUNT_POINT/Terminal/Linux"
cp GIT_FOLDER/backup.sh .
sed -i 's/\r//' backup.sh > /dev/null 2>&1
./backup.sh -r

#clone additional meta sources
cd layers
git clone $MONO_SOURCES
git clone --no-checkout $META_JAVA_SOURCES
cd meta-java; git checkout -b java $META_JAVA_BRANCH; cd .. 
git clone $NODE_SOURCES
git clone $NODE_CONTRIBS
git clone $MOSQUITTO_SOURCES

