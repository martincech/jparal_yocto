FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += " \
   file://95-usb.rules \
   file://wvdial.conf \
   file://usb-modem.service \ 
"
do_install_append(){
	install -d ${D}/etc
	install -m 0755 ${WORKDIR}/wvdial.conf ${D}/etc/
	
	install -d ${D}/etc/udev/rules.d
	install -m 0755 ${WORKDIR}/95-usb.rules ${D}/etc/udev/rules.d/
	
	install -d ${D}/etc/systemd/system
	install -m 0755 ${WORKDIR}/usb-modem.service ${D}/etc/systemd/system/
}
