SUMMARY = "Node-Red nodes for obtaining cpu system information."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-os"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/Argonne-National-Laboratory/node-red-contrib-os.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
