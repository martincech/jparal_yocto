SUMMARY = "C client for Apache Kafka"
HOMEPAGE = "https://github.com/edenhill/librdkafka"
SECTION = "libs"
DEPENDS = "zlib libpthread-stubs"

SRC_URI = " \
    git://github.com/edenhill/librdkafka.git;rev=master \
    file://cxx.patch \
"
S = "${WORKDIR}/git"
LICENSE = "LIC"
LIC_FILES_CHKSUM = "file://LICENSE;md5=6baccb9e4e9e0044093be8d78d0a1087"

inherit pkgconfig

RDEPENDS_${PN} += "libcrypto libssl"

FILES_SOLIBSDEV = ""

CONFIGUREOPTS = " --build=${BUILD_SYS} \
		  --host=${HOST_SYS} \
		  --target=${TARGET_SYS} \
		  --prefix=${STAGING_DIR_TARGET}${prefix} \
		  --exec_prefix=${STAGING_DIR_TARGET}${exec_prefix} \
		  --bindir=${STAGING_DIR_TARGET}${bindir} \
		  --sbindir=${STAGING_DIR_TARGET}${sbindir} \
		  --libexecdir=${STAGING_DIR_TARGET}${libexecdir} \
		  --datadir=${STAGING_DIR_TARGET}${datadir} \
		  --sysconfdir=${STAGING_DIR_TARGET}${sysconfdir} \
		  --sharedstatedir=${STAGING_DIR_TARGET}${sharedstatedir} \
		  --localstatedir=${STAGING_DIR_TARGET}${localstatedir} \
		  --libdir=${STAGING_DIR_TARGET}${libdir} \
		  --includedir=${STAGING_DIR_TARGET}${includedir} \
		  --infodir=${STAGING_DIR_TARGET}${infodir} \
		  --mandir=${STAGING_DIR_TARGET}${mandir} \
                  --arch=${TARGET_ARCH} \
		  "

do_configure(){   
   	${S}/configure ${CONFIGUREOPTS}
}

do_install(){ 
        cd ${S}
	oe_runmake 'DESTDIR=${D}'  	
 	cd ${S}/src
	
	install -d ${D}${libdir} 		
	install -m 775 librdkafka.so.1 ${D}${libdir}/	
        install -m 775 librdkafka.so.1 ${D}${libdir}/librdkafka.so 	
}

FILES_${PN} += "${libdir}/librdkafka*"
