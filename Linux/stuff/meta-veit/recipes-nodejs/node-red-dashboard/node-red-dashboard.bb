SUMMARY = "A set of dashboard nodes for Node-RED"
HOMEPAGE = "https://flows.nodered.org/node/node-red-dashboard"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global gulp

SRC_URI="\
        git://github.com/node-red/node-red-dashboard.git;rev=77c6a3c6fcbb95f602644914b8c9a61b61cc076d \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "