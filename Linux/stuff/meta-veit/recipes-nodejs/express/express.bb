SUMMARY = "Fast, unopinionated, minimalist web framework"
HOMEPAGE = "http://expressjs.com/"
LICENSE = "CLOSED"

DEPENDS = "nodejs"

#install npm package
inherit npm-install-global

SRC_URI="\
        git://github.com/expressjs/express.git;rev=master\
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "

