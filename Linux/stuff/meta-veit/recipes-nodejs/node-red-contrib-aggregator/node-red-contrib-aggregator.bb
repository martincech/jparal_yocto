SUMMARY = "A Node-Red node that can be used to aggregate numeric values over a specific time span"
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-aggregator"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/jaggr2/node-red-contrib-aggregator.git;rev=master \		
		file://${PN}.patch \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
