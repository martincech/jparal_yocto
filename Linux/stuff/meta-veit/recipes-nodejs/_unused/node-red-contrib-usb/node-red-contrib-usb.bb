SUMMARY = "A set of node-red nodes used as usb utilities"
HOMEPAGE = "http://flows.nodered.org/node/node-red-contrib-usb"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/rajeshsola/node-red-addons.git;rev=master \
"
S = "${WORKDIR}/git/node-red-contrib-usb/"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
