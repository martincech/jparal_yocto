SUMMARY = "Sensor pack data collector"
LICENSE = "CLOSED"

SP_COLLECTOR_SERVICE="LinuxTerminal.service"
SERVICES="${SP_COLLECTOR_SERVICE}"

DEPENDS = "libnl openssl mono"
INITSCRIPT_NAME = "${SP_COLLECTOR_SERVICE}"
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${SP_COLLECTOR_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"
SP_COLLECTOR="LinuxTerminal.exe"

SRC_URI = "\	
        file://LinuxTerminal.tar \
        file://LinuxTerminal.service \	
"

COLLECTOR_PATH="/usr/sp-collector"
S_B ="${D}${COLLECTOR_PATH}"
S_B_SCOURCE_D="${WORKDIR}/LinuxTerminal"

do_install() {
    install -d ${S_B}  
    install -d ${S_B}/x64 
    install -d ${S_B}/x86 
    install -m 0755 ${S_B_SCOURCE_D}/*.* ${S_B}/ 
    install -m 0755 ${S_B_SCOURCE_D}/x64/* ${S_B}/x64/ 
    install -m 0755 ${S_B_SCOURCE_D}/x86/* ${S_B}/x86/     


    install -d ${D}${systemd_unitdir}/system    
    install -m 0755 ${WORKDIR}/${SP_COLLECTOR_SERVICE} ${D}${systemd_unitdir}/system/

    for prop in ${D}${systemd_unitdir}/system/${SP_COLLECTOR_SERVICE}; 
    do
      sed -i -e "s,@COLLECTOR_PATH@,"${COLLECTOR_PATH}",g" -e "s,@SP_COLLECTOR@,"${SP_COLLECTOR}",g" ${prop}
    done    
}

FILES_${PN} = "\   
  ${COLLECTOR_PATH}/* \
  ${COLLECTOR_PATH}/x64/* \
  ${COLLECTOR_PATH}/x86/* \"
