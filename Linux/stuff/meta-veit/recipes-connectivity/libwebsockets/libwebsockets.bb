SUMMARY = "libwebsockets library"

LICENSE = "CLOSED"
DEPENDS += "zlib openssl"

SRC_URI = "\
          git://github.com/warmcat/libwebsockets.git;rev=master \
"
inherit cmake
S = "${WORKDIR}/git"

FILES_${PN} = "\  
   ${libdir}/*.so* \
"