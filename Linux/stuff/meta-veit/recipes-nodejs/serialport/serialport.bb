SUMMARY = "Node.js package to access serial ports."
HOMEPAGE = "https://www.npmjs.com/package/serialport"
LICENSE = "CLOSED"

DEPENDS = "nodejs"

#install npm package
inherit npm-install-global

SRC_URI="\
        git://github.com/EmergingTechnologyAdvisors/node-serialport.git;tag=4.0.7;branch=4x \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "

