SUMMARY = "A Node-RED node to create a counter with messages."
HOMEPAGE = "https://flows.nodered.org/node/node-red-contrib-counter"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/eisbehr-/node-red-counter.git;rev=master \		
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
