SUMMARY = "Mirror maker definition for Kafka"
LICENSE = "CLOSED"

require ../kafka-common.inc

MIRROR_SERVICE="${PN}.service"
SERVICES="${MIRROR_SERVICE}"

DEPENDS = "libnl openssl zookeeper broker mono"
INITSCRIPT_NAME = "${MIRROR_SERVICE}"
inherit update-rc.d systemd
SYSTEMD_SERVICE_${PN} = "${MIRROR_SERVICE}"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"


DEFAULT_PREFERENCE = "-1"

MIRROR_MAKER_FOLDER_NAME="mirror-maker"

SRCREV = "${AUTOREV}"
SRC_URI += "\
        file://config.json \
        file://KafkaMirrorMaker.exe.config \
        file://${MIRROR_MAKER_FOLDER_NAME}.tar \
        file://${MIRROR_SERVICE} \        
"

MIRROR_MAKER="KafkaMirrorMaker.exe"
MIRROR_MAKER_SOURCE_D = "${WORKDIR}/${MIRROR_MAKER_FOLDER_NAME}"
MIRROR_MAKER_PATH="${KAFKA_PATH}/${MIRROR_MAKER_FOLDER_NAME}"
M_B = "${D}/${MIRROR_MAKER_PATH}"

SED_FILES+="${M_B}/*.config"

do_install() {  
    install -d ${CONFIG_PATH}
    install -m 0755 ${WORKDIR}/*.json ${CONFIG_PATH}    

    install -d ${M_B}
    install -m 0755 ${MIRROR_MAKER_SOURCE_D}/* ${M_B}
    install -m 0755 ${WORKDIR}/*.config ${M_B}  
}