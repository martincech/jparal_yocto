SUMMARY = "Node-red for Node.js"
HOMEPAGE = "https://nodered.org/"
LICENSE = "CLOSED"

DEPENDS = "nodejs"
inherit npm-install-global grunt


SRC_URI="\
        git://github.com/node-red/node-red.git;rev=master \
"
S = "${WORKDIR}/git"


do_install_append() {
  oe_runnpm run build
}

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* \
"

