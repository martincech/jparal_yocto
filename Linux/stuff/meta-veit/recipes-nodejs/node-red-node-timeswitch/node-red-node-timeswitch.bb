SUMMARY = "A Node-RED node to provide a simple timeswitch to schedule daily on/off events."
HOMEPAGE = "https://flows.nodered.org/node/node-red-node-timeswitch"
LICENSE = "CLOSED"

DEPENDS = "node-red"
inherit npm-install-global

SRC_URI="\
        git://github.com/node-red/node-red-nodes.git;rev=master \
"
S = "${WORKDIR}/git/time/timeswitch"


FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* "
