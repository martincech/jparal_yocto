SUMMARY = "A pure JavaScript implemetation of MODBUS-RTU (and TCP) for NodeJS."
HOMEPAGE = "https://www.npmjs.com/package/modbus-serial"
LICENSE = "CLOSED"

DEPENDS = "nodejs serialport"

#install npm package
inherit npm-install-global

SRC_URI="\
        git://github.com/yaacov/node-modbus-serial;rev=master \
		file://${PN}.patch \
"
S = "${WORKDIR}/git"

FILES_${PN} = "\   
   ${libdir}/node_modules/${PN}/* \
   ${bindir}/${PN}* "

